<div class="footer">
	<div class="container">
		<div class="footer-top">
			<ul class="social-in">
				<li class="animated zoomIn delay2"><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
				<li class="animated zoomIn delay2"><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
				<li class="animated zoomIn delay2"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>				
				
			</ul>
			
			<p class="footer-class">Copyright © 2017 vive24 <a href="http://w3layouts.com/" target="_blank">Wilmer Martínez</a> </p>
		</div>
	</div>	
	<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</div>