<?php 
	require('menu.php');
	require_once('consultas/conexion.php');
	$id = $_GET['id'];


$conn = new Conexion();

$llamarMetodo = $conn->Conectar();

$sql = "SELECT * FROM tbl_local where categoria_id='$id'";
$stmt = $llamarMetodo->prepare($sql);
$stmt->execute();
 ?>
<!---->
<div class="content">
	<div class="container">	
		<div class="load_more">	
			 <ul id="myList" class="col-md-12">
			    <!-- These are our grid blocks -->
			    		    	<?php 
		    	 while ($row=$stmt->fetch()) {
		    	 ?>
			<li class="col-md-4 col-sm-4 col-xs-12 espacio_result fondo_result animated bounceInLeft delay2">
				<article class="contenedor_establecimiento bordes bounceInDown delay">
					<div class="col-md-4 col-sm-12 col-xs-12 quitar_padding"><figure class="ancho"><?php echo '<img src="../back/'.$row[5].'">' ?></figure></div>
					<div class="col-md-8 col-sm-12 col-xs-6">

						<!--funcion para limitar el numero de caracteres-->
						<?php 

						$rest = substr("$row[1]", 0, 20);
							$cantidad = strlen($rest); 
							if ($cantidad >=20 ) {
								$nombre = $rest.'.....';
							}else{
								$nombre = $rest;
							}


						$desc = substr("$row[4]",0,23);
							$cantidad2 = strlen($desc);
							if ($cantidad2>=23) {
								$descripcion = $desc.'...';
							}else{
								$descripcion = $desc;
							}

						?>

					<h4 class="espacio_titulo"><strong class="morado" maxlength="6"><?php echo $nombre ?></strong></h4>
					<p><i class="fa fa-map-marker morado" aria-hidden="true"></i> <?php echo $row[2] ?></p>
					<p><i class="fa fa-phone morado" aria-hidden="true"></i> <?php echo $row[3] ?></p>
					<p><i class="fa fa-building morado" aria-hidden="true"></i> <?php echo $descripcion ?></p>
					</div>
					<?php  echo '<a href="details.php?id='.$row[0].'" class="btn btn-success boton_ancho" role="button"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> VER MÁS</a>' ?>

				</article>
			</li>
			<?php } ?>
		<div class="clearfix"> </div>
	</ul>
	<div id="loadMore"><button class="btn btn-danger"> <i class="fa fa-plus" aria-hidden="true"></i>
 VER MÁS ESTABLECIMIENTOS</button></div>
		</div>
		<!---->
		
	</div>
</div>
<?php 

	require('footer.php');
 ?>
</body>
</html>