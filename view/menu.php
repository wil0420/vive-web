<?php 
	require_once('consultas/conexion.php');
	$conn = new Conexion();
$llamarMetodo = $conn->Conectar();

$sql = "SELECT * FROM tbl_categoria";
$stmt = $llamarMetodo->prepare($sql);
$stmt->execute();
 ?>
<!DOCTYPE html>
<html>
<head>
<title>Vive 24</title>
<link rel='shortcut icon' type='image/x-icon' href='../imagenes/favicon.ico' />

<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/estilo.css" rel="stylesheet" type="text/css" media="all" />	
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" href="css/font-awesome-animation.min.css">


<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Constellation Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
 <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!---->
<script>
		$(document).ready(function () {
		    size_li = $("#myList li").size();
		    x=6;
		    $('#myList li:lt('+x+')').show();
		    $('#loadMore').click(function () {
		        x= (x+3 <= size_li) ? x+3 : size_li;
		        $('#myList li:lt('+x+')').show();
		    });
		});
	</script>
<!--oncontextmenu="return false" ondragstart="return false" onkeydown="return false" onselectstart="return false-->
</head>
<body>
<div class="header">
	<div class="container">	
	<div class="header-on">
			<div class="header-left">
				<div class="menu">												
						<a href="#" class="right_bt" id="activator"><i class="fa fa-bars menu_icon morado" aria-hidden="true"></i></a>
							<div class="box" id="box">
								<div class="box_content">
								   <div class="menu_box_list">
								  <h3> Hello, Vive_24!</h3>
									   <ul>
									   	  <?php 
              							while ($row=$stmt->fetch()) {
               								 ?>
											<?php echo '<li class=""><a class="faa-parent animated-hover" href="result.php?id='.$row[0].'" >'.$row[2].' '.$row[1].'</a></li>' ?>
											<?php } ?>								
											<li class=""><a href="contact.php" class="scroll morado"><i class="fa fa-envelope-o" aria-hidden="true"></i> contacto </a></li>
											<div class="clearfix"></div>
										</ul>
									</div>
									<a class="boxclose" id="boxclose"><img src="images/cross.png" alt=""/></a>
								</div>                
							</div>
								 <script type="text/javascript">
										var $ = jQuery.noConflict();
											$(function() {
												$('#activator').click(function(){
														$('#box').animate({'left':'0px'},500);
												});
												$('#boxclose').click(function(){
														$('#box').animate({'left':'-700px'},500);
												});
											});
											$(document).ready(function(){
											
											//Hide (Collapse) the toggle containers on load
											$(".toggle_container").hide(); 
											
											//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
											$(".trigger").click(function(){
												$(this).toggleClass("active").next().slideToggle("slow");
												return false; //Prevent the browser jump to the link anchor
											});
											
											});
										</script>
				</div> 
				</div>
	<!---->	
		<div class="search">		
			<a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="fa fa-search morado" aria-hidden="true"></i> Buscar....</a>
		</div>
	<div class="clearfix"> </div>
	</div>
	<!---pop-up-box---->
					  <script type="text/javascript" src="js/modernizr.custom.min.js"></script>    
					<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
				<div id="small-dialog" class="mfp-hide">
				<div class="search-top">
						<div class="login">
							<form action="search.php" method="POST">
							<!--<i class="fa fa-search" aria-hidden="true"></i><input type="submit" value="">-->
							<input name="parametro" type="text" value="Que estas buscando?" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">	
							</form>	
						</div>
							<p></p>
					</div>				
				</div>
				 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>			
	<!---->
		<div class="header-top">		
			<a href="index.php"><img src="../imagenes/logo.png" class="logo animated jackInTheBox delay"></a>
			<p class="animated bounceInLeft delay2">Encuentra el lugar a la hora que necesites</p>
		</div>
	</div>
</div>