<?php 
	require('menu.php');
	require_once('consultas/conexion.php');
	$id = $_GET['id'];


$conn = new Conexion();

$llamarMetodo = $conn->Conectar();

$sql = "SELECT * FROM tbl_local where id='$id'";
$stmt = $llamarMetodo->prepare($sql);
$stmt->execute();
 ?>
<!---->
<div class="content">
	<div class="container">	
		<div class="load_more">
			   		<?php 
		    	 while ($row=$stmt->fetch()) {
		    	 ?>	
			<div class="row fondo_content">
				<div class="col-md-12 fondo_titulo text-center">
					<article class="col-md-3">
					<figure class="ancho2"><?php echo '<img src="../back/'.$row[5].'">' ?></figure>
					</article>
					<article class="col-md-9">
					<h2 class="morado titulo_details" id="titulo"><strong><?php echo $row[1] ?></strong></h2>
					</article>
				</div>
				<div class="row">
				<div class="col-md-6 espacio altura">
					<article class="tamaño_texto col-md-12 grid">
					<p><i class="fa fa-map-marker morado" aria-hidden="true"></i>&nbsp <?php echo $row[2] ?></p>
					<p><i class="fa fa-phone morado" aria-hidden="true"></i>&nbsp <?php echo $row[3] ?></p>
					<p><i class="fa fa-globe morado" aria-hidden="true"></i>&nbsp <a href="<?php echo $row[9] ?>" target="_blank"><?php echo $row[9] ?></a></p>
					</article>
				</div>
				<div class="col-md-6 espacio grid altura">
					<article class="tamaño_texto col-md-12">
					<p class="text-justify"><?php echo $row[4] ?></p>
					</article>
				</div>
				</div>
				<div class="col-md-6 grid">
					<figure class="foto"><?php echo '<img id="myImg" src="../back/'.$row[6].'">' ?></figure>
				</div>
				<div class="col-md-6 grid">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3977.0597964018616!2d-74.09888968584289!3d4.583287343942133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNMKwMzQnNTkuOCJOIDc0wrAwNSc0OC4xIlc!5e0!3m2!1ses!2sco!4v1511753085673" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>

			<?php } ?>
				

		</div>
		<!---->
		
	</div>
</div>
<?php 

	require('footer.php');
 ?>
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>


<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>
</body>
</html>