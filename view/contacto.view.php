<?php require('menu.php') ?>
<!---->
	<div class="container">	
		<div class="contact">				
			<div class="contact-grids">
				<div class="contact-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.943479231848!2d-74.07687888584286!3d4.604143643770693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f99a0f9bb0ce7%3A0x5d71ff487837183c!2sUniempresarial!5e0!3m2!1ses!2sco!4v1512355028777" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="top-contact">
					<div class="col-md-4 contact-right">
						<h3>Información de contacto</h3>
						<li class=" lista"><i class="fa fa-map-marker espacio2 morado" aria-hidden="true"></i> Bogotá D.C</li>
						<li class=" lista"><i class="fa fa-mobile espacio2 morado" aria-hidden="true"></i> B1010101010</li>
						<li class=" lista"><i class="fa fa-envelope espacio2 morado" aria-hidden="true"></i> correo@vive24.com</li>
						
					</div>	
					<div class="col-md-8 contact-form">
							<h3>Contáctenos</h3>
						<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method= "post">
							<div class="contact-in">
								<div class="name-in">
									<span>Nombre:</span>
									<input type="text" value="" name="nombre">
								</div>
								<div class="name-in">
									<span>Email:</span>
									<input type="text" value="" name="correo">
								</div>
								<div class="clearfix"> </div>
							</div>
								<div class="name-on">
									<span>Mensaje:</span>
									<textarea name="mensaje"></textarea>
								</div>
								<?php if (!empty($errores)): ?>
								<div class="alert alert-danger">
									<?php echo  $errores; ?>
								</div>
							<?php elseif ($enviado): ?>
								<div class="alert alert-success">
									<p>Enviado correctamente</p>
								</div>
							<?php endif ?>
								<input type="submit" value="Enviar" class="morado" name="submit">
						</form>
					</div>	
					<div class="clearfix"> </div>
				</div>	
			</div>
		</div>
	</div>
<?php require('footer.php') ?>
</body>
</html>