 <!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	<!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
</head>
<body class="fondo_textura">
	<section id="contenedor" class="row">
		<article class="col-md-4"></article>
		<article class="col-md-4">
		<img src="../imagenes/logo.png" class="logo">
		<h3 class="fuente title_login">Ingresar al administrador</h3>
		<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" name="login">
			<div class="input-group form-group">
				    <span class="input-group-addon"><i class="fa fa-envelope morado" aria-hidden="true"></i></span><input type="email" name="correo" placeholder="Ingrese su correo" required class="form-control">
			</div>
			<div class="input-group form-group">
				<span class="input-group-addon"><i class="fa fa-key morado" aria-hidden="true"></i>
</span><input type="password" name="password" placeholder="contraseña" required class="form-control">
			</div>
			<button onclick="login.submit()" class="espacio boton_enviar"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <span class="fuente">Ingresar</span></button>

			<?php if (!empty($errores)):?>
				<ul class="quitar_lista">
					<?php echo $errores; ?>
				</ul>

			<?php endif; ?>
		</form>
		</article>
		<article class="col-md-4"></article>
	</section>
</body>
</html>