<?php session_start();

if (isset($_SESSION['correo'])) {

 } else{
  header('location: ../login/login.php');
 }

require_once('conexion.php');
require_once('consultas.php');
 ?>

 <!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Vive 24</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendor/devicons/css/devicons.min.css" rel="stylesheet">
    <link href="../vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/resume.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">


  </head>

  <!--oncontextmenu="return false" ondragstart="return false" onkeydown="return false" onselectstart="return false-->

  <body id="page-top"">
    <?php 
    require 'menu.php';

    $usuario = $_SESSION['correo'];

     ?>
    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column fondo_textura" id="about">
        <section id="contenedor" class="row">
        <article class="col-md-2"></article>
        <article class="col-md-8 centrar">
        <h3 class="morado bold">INGRESAR LOS DATOS DEL ESTABLECIMIENTO</h3>
				<form action="insertar_local.php" method="post" enctype= "multipart/form-data" class="form-group">
          <div class="form-group">
                <label class="col-form-label" for="formGroupExampleInput">INGRESE EL NOMBRE DEL ESTABLECIMIENTO</label>
                <input class="form-control" type="text" name="nombre" placeholder="Ingresar nombre del local" class="espacio cajas_datos" required>
          </div>
          <div class="form-group">
                <label class="col-form-label" for="formGroupExampleInput">INGRESE LA DIRECCIÓN DEL ESTABLECIMIENTO</label>
				        <input class="form-control" type="text" name="direccion" placeholder="Ingrese la dirección" class="espacio cajas_datos" required>
          </div>
          <div class="form-group">
                <label class="col-form-label" for="formGroupExampleInput">INGRESE EL TELÉFONO DEL ESTABLECIMIENTO</label>
				        <input class="form-control" type="number" name="telefono" placeholder="Ingrese teléfono" class="espacio cajas_datos" required>
          </div>
          <div class="form-group">
                <label class="col-form-label" for="formGroupExampleInput">INGRESE LA DESCRIPCIÓN</label>
				        <textarea class="form-control" name="descripcion" placeholder="Descripción del local" class="espacio cajas_datos" required></textarea>
          </div>
          <div class="form-group">
        				<label class="col-form-label" for="formGroupExampleInput">SELECCIONE EL LOGO DEL ESTABLECIMIENTO</label>
        				<input class="form-control" type="file" name="logo" class="espacio cajas_datos" required>
        </div>
        <div class="form-group">
              <label class="col-form-label" for="formGroupExampleInput">SELECCIONE UNA FOTO DEL ESTABLECIMIENTO</label>
				      <input class="form-control" type="file" name="foto" class="espacio cajas_datos" required>
        </div>
        <div class="form-group">
              <label class="col-form-label" for="formGroupExampleInput">INGRESE LA LONGITUD</label>
				      <input class="form-control" type="tex" name="longitud" placeholder="longitud" class="espacio cajas_datos">
        </div>
        <div class="form-group">
              <label class="col-form-label" for="formGroupExampleInput">INGRESE LA LATITUD</label>
				      <input class="form-control" type="text" name="latitud" placeholder="latitud" class="espacio cajas_datos" required>
        </div>
        <div class="form-group">
              <label class="col-form-label" for="formGroupExampleInput">INGRESE LA PÁGINA WEB</label>
				      <input class="form-control" type="text" name="link" placeholder="link pagina web" class="espacio cajas_datos" required>
        </div>
        <div class="form-group">
              <input class="form-control" type="hidden" name="usuario" placeholder="link pagina web" class="espacio cajas_datos" value="<?php echo $usuario ?>">
        </div>
        <div class="form-group">
				      <label class="col-form-label" for="formGroupExampleInput">SELECCIONE LA CATEGORIA</label>
      				<select class="form-control" name="categoria">
      				<?php 
      				foreach ($result as $row){
      				 ?>
      				 	<option value="<?php echo $row['id'] ?>"><?php echo $row['nombre'] ?></option>
      				<?php 
      					}
      				 ?>
      				</select>
				<input type="submit" name="enviar" value="Enviar datos" class="espacio boton_enviar">
        </div>
		</form>
    </article>
      <article class="col-md-2"></article>
	</section>
</section>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../js/resume.min.js"></script>

  </body>

</html>
