


    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="../js/gmap3.js"></script>
    <style>
      
      .gmap3{
        
        width: 100%;
        height: 500px;
      }
    </style>

    
    <script type="text/javascript">
        
      $(function(){
      
        $('#mapa').gmap3({
          map:{
            options:{
              center:[4.698866,-74.089336],
              zoom: 10

            }
          },
          marker:{
            values:[
             {latLng:[4.698866,-74.089336], data:"<h4>ejemplo</h4>", options:{icon: "http://maps.google.com/mapfiles/marker_grey.png"}},




              ],
            options:{
              draggable: false

            },
            events:{
              mouseover: function(marker, event, context){
                var map = $(this).gmap3("get"),
                  infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                  infowindow.open(map, marker);
                  infowindow.setContent(context.data);
                } else {
                  $(this).gmap3({
                    infowindow:{
                      anchor:marker, 
                      options:{content: context.data}
                    }
                  });
                }
              },
              onclick: function(){
                var infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                  infowindow.close();
                }
              }
            }
          }
        });
      });
    </script>

    <div id="mapa" class="gmap3"></div>
    
