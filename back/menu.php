    <nav class="navbar navbar-expand-lg navbar-dark fondo fixed-top" id="sideNav">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
        <span class="d-block d-lg-none">Vive 24</span>
        <span class="d-none d-lg-block">
          <p class="nombre_usuario_session">Bienvenido | <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo $_SESSION['correo']; ?></p>
          <img class="img-fluid img-profile mx-auto mb-2" src="../imagenes/logo.png" alt="">
        </span>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="ingresar_local.view.php"><i class="fa fa-building" aria-hidden="true"></i> Ingresar local</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="ver_categorias.php"><i class="fa fa-eye" aria-hidden="true"></i>
Ver locales</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="editar_local.php"><i class="fa fa-pencil" aria-hidden="true"></i>
Editar locales</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="../login/cerrar.php"><i class="fa fa-sign-out" aria-hidden="true"></i>
 Cerrar Sesión</a>
          </li>
        </ul>
      </div>
    </nav>