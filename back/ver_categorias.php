<?php session_start();

if (isset($_SESSION['correo'])) {
 } else{
  header('location: ../login/login.php');
 }
require_once('conexion.php');

$conn = new Conexion();
$llamarMetodo = $conn->Conectar();

$sql = "SELECT * FROM tbl_categoria";
$stmt = $llamarMetodo->prepare($sql);
$stmt->execute();
 ?>

 <!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Vive 24</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendor/devicons/css/devicons.min.css" rel="stylesheet">
    <link href="../vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/resume.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">


  </head>

  <body id="page-top">
        <?php 
    require 'menu.php';

     ?>

    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
      	<section>
     	<h3 class="verde">Seleccione la categoria para ver locales</h3>
        <ul class="nav-var">
           <?php 
              while ($row=$stmt->fetch()) {
                ?>
        	<li class="nav-item quitar_lista">	
				<?php echo '<a class="nav-link js-scroll-trigger" href="caracteristicas.php?id='.$row[0],$row[1].'"><strong>'.$row[0].' :</strong> '.$row[1].'</a>'; ?>
			</li> 
      <?php 
        }
       ?>       	
        </ul>
        </section>
      </section>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../js/resume.min.js"></script>

  </body>

</html>
