<?php session_start();

if (isset($_SESSION['correo'])) {
 } else{
  header('location: ../login/login.php');
 }


require_once('conexion.php');


$conn = new Conexion();

$llamarMetodo = $conn->Conectar();

$sql = "SELECT * FROM tbl_local";
$stmt = $llamarMetodo->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll();

 ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Vive 24</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendor/devicons/css/devicons.min.css" rel="stylesheet">
    <link href="../vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/resume.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">


  </head>

  <body id="page-top">
        <?php 
    require 'menu.php';

     ?>

    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
      	<section class="col-md-12 col-sm-12 col-xs-12">
      		<h2>EDITAR LOCALES</h2>
  			<p>Elige el local a editar la información:</p>            
		  <table class="table">
		    <thead>
		      <tr>
		        <th>ID</th>
		        <th>Nombre</th>
		        <th>Dirección</th>
		        <th>Teléfono</th>
		        <th>Descripción</th>
		        <th>Latitud</th>
		        <th>Longitud</th>
		        <th>Página</th>
            <th>categoria</th>
		        <th>Editar</th>
            <th>Eliminar</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php 
		    	   foreach ($result as $row) {
		    	 ?>
		      <tr>
		      	<td><?php echo $row[0] ?></td>
		      	<td><?php echo $row[1] ?></td>
		      	<td><?php echo $row[2] ?></td>
		      	<td><?php echo $row[3] ?></td>
		      	<td><?php echo $row[4] ?></td>
		      	<td><?php echo $row[7] ?></td>
		      	<td><?php echo $row[8] ?></td>
		      	<td><a href="<?php echo $row[9] ?>" target="_blank"><?php echo $row[9] ?></a></td>
            <td><?php echo $row[10] ?></td>
		      	<td><?php 
		      		echo '<a class="editar" href="editar.view.php?id='.$row[0].'">Editar</a>';
		      	 ?></td>
             <td>
               
                <?php echo '<a class="eliminar" href="eliminar_local.php?id='.$row[0].'" onclick="confirmar()">Eliminar</a>'; ?>
             </td>
             <?php } ?>
		      </tr>
		    </tbody>
		  </table>
		 </section>
      </section>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../js/resume.min.js"></script>
<script>
function confirmar(){
  var r = confirm("¿Esta seguro de eliminar este local?");
  if (r == true){
    return true;
  } else {
    alert('Nooooooooooo');
  }
}
</script>

  </body>

</html>